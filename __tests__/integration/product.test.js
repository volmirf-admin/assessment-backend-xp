import request from 'supertest';
import app from '../../src/app';

describe('CRUD Product', () => {
  const product = {
    name: 'A12',
    sku: '42254-011',
    price: 89.9,
    description: 'Bola de Basquete',
    quantity: 22,
  };
  const category = {
    code: 'A12',
    name: 'Esportes',
  };

  it('should create a product', async () => {
    const response = await request(app).post('/products').send(product);
    expect(response.status).toBe(201);

    expect(response.body.name).toBe(product.name);
    expect(response.body.sku).toBe(product.sku);
    expect(response.body.price).toBe(product.price);
    expect(response.body.description).toBe(product.description);
    expect(response.body.quantity).toBe(product.quantity);

    expect(response.body).toHaveProperty('_id');
    expect(response.body).toHaveProperty('createdAt');
    expect(response.body).toHaveProperty('updatedAt');

    product._id = response.body._id;

    const responseCategory = await request(app)
      .post('/categories')
      .send(category);
    category._id = responseCategory.body._id;

    let newProductWithCategory = await request(app)
      .post('/products')
      .send({
        ...product,
        categories: [responseCategory.body],
        _id: undefined,
      });

    expect(newProductWithCategory.body).toHaveProperty('categories');
    expect(newProductWithCategory.body.categories).toBeInstanceOf(Array);

    expect(newProductWithCategory.body.categories[0].code).toBe(category.code);
    expect(newProductWithCategory.body.categories[0].name).toBe(category.name);
  });

  it('should find the product created', async () => {
    const response = await request(app).get(`/products/${product._id}`);
    expect(response.status).toBe(200);

    expect(response.body.name).toBe(product.name);
    expect(response.body.sku).toBe(product.sku);
    expect(response.body.price).toBe(product.price);
    expect(response.body.description).toBe(product.description);
    expect(response.body.quantity).toBe(product.quantity);

    expect(response.body).toHaveProperty('_id');
    expect(response.body).toHaveProperty('createdAt');
    expect(response.body).toHaveProperty('updatedAt');
  });

  it('should find a list of products with the product created', async () => {
    const response = await request(app).get('/products');
    expect(response.status).toBe(200);

    expect(response.body).toBeInstanceOf(Array);

    expect(response.body.find((e) => e._id === product._id)).toBeTruthy();
  });

  it('should update the product', async () => {
    product.name = 'New name';
    product.sku = '11111-111';
    product.price = 33.6;
    product.description = 'new description';
    product.quantity = 10;
    product.categories = [];

    let responseUpdate = await request(app)
      .put(`/products/${product._id}`)
      .send(product);
    expect(responseUpdate.status).toBe(200);

    expect(responseUpdate.body.name).toBe(product.name);
    expect(responseUpdate.body.sku).toBe(product.sku);
    expect(responseUpdate.body.price).toBe(product.price);
    expect(responseUpdate.body.description).toBe(product.description);
    expect(responseUpdate.body.quantity).toBe(product.quantity);

    expect(responseUpdate.body).toHaveProperty('_id');
    expect(responseUpdate.body).toHaveProperty('createdAt');
    expect(responseUpdate.body).toHaveProperty('updatedAt');

    expect(responseUpdate.body._id).toBe(product._id);

    const responseGet = await request(app).get(`/products/${product._id}`);
    expect(responseGet.status).toBe(200);

    expect(responseUpdate.body.name).toBe(product.name);
    expect(responseUpdate.body.sku).toBe(product.sku);
    expect(responseUpdate.body.price).toBe(product.price);
    expect(responseUpdate.body.description).toBe(product.description);
    expect(responseUpdate.body.quantity).toBe(product.quantity);

    expect(responseUpdate.body.categories).toHaveLength(0);

    product.categories.push(category);

    responseUpdate = await request(app)
      .put(`/products/${product._id}`)
      .send(product);

    expect(responseUpdate.body.categories).toHaveLength(1);

    expect(responseUpdate.body.categories[0].code).toBe(category.code);
    expect(responseUpdate.body.categories[0].name).toBe(category.name);

    expect(responseUpdate.body.categories[0]).toHaveProperty('_id');
    expect(responseUpdate.body.categories[0]).toHaveProperty('createdAt');
    expect(responseUpdate.body.categories[0]).toHaveProperty('updatedAt');
  });

  it('should delete the category', async () => {
    const responseUpdate = await request(app).delete(
      `/products/${product._id}`
    );
    expect(responseUpdate.status).toBe(200);

    const responseGet = await request(app).get(`/products/${product._id}`);
    expect(responseGet.status).toBe(404);
  });
});

describe('CRUD Category - Exceptions expected', () => {
  const product = {
    name: 'A12',
    sku: '42254-011',
    price: 89.9,
    description: 'Bola de Basquete',
    quantity: 22,
  };

  it('should send a error when trying to create a product without a mandatory field', async () => {
    const temporaryProduct = Object.assign(product);
    temporaryProduct.name = undefined;
    let response = await request(app).post('/products').send(temporaryProduct);
    expect(response.status).toBe(400);

    temporaryProduct.name = product.name;
    temporaryProduct.sku = undefined;
    response = await request(app).post('/products').send(temporaryProduct);
    expect(response.status).toBe(400);

    temporaryProduct.sku = product.sku;
    temporaryProduct.price = undefined;
    response = await request(app).post('/products').send(temporaryProduct);
    expect(response.status).toBe(400);

    temporaryProduct.price = product.price;
    temporaryProduct.description = undefined;
    response = await request(app).post('/products').send(temporaryProduct);
    expect(response.status).toBe(400);

    temporaryProduct.description = product.description;
    temporaryProduct.quantity = undefined;
    response = await request(app).post('/products').send(temporaryProduct);
    expect(response.status).toBe(400);
  });

  it('should send a error when trying to update a product that does not exist', async () => {
    let response = await request(app).put(`/products/invalidId`);
    expect(response.status).toBe(400);

    response = await request(app).put(`/products/000000000000000000000000`);
    expect(response.status).toBe(404);
  });

  it('should send a error when trying to delete a product that does not exist', async () => {
    let response = await request(app).delete(`/products/invalidId`);
    expect(response.status).toBe(400);

    response = await request(app).delete(`/products/000000000000000000000000`);
    expect(response.status).toBe(404);
  });

  it('should send a error when trying to find a product that does not exist', async () => {
    let response = await request(app).get(`/products/invalidId`);
    expect(response.status).toBe(400);

    response = await request(app).get(`/products/000000000000000000000000`);
    expect(response.status).toBe(404);
  });
});
