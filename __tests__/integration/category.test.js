import request from 'supertest';
import app from '../../src/app';

describe('CRUD Category', () => {
  const category = {
    code: 'A12',
    name: 'Esportes',
  };

  it('should create a category', async () => {
    const response = await request(app).post('/categories').send(category);
    expect(response.status).toBe(201);

    expect(response.body.code).toBe(category.code);
    expect(response.body.name).toBe(category.name);

    expect(response.body).toHaveProperty('_id');
    expect(response.body).toHaveProperty('createdAt');
    expect(response.body).toHaveProperty('updatedAt');

    category._id = response.body._id;
  });

  it('should find the category created', async () => {
    const response = await request(app).get(`/categories/${category._id}`);
    expect(response.status).toBe(200);

    expect(response.body.code).toBe(category.code);
    expect(response.body.name).toBe(category.name);

    expect(response.body).toHaveProperty('_id');
    expect(response.body).toHaveProperty('createdAt');
    expect(response.body).toHaveProperty('updatedAt');
  });

  it('should find a list of categories with the category created', async () => {
    const response = await request(app).get('/categories');
    expect(response.status).toBe(200);

    expect(response.body).toBeInstanceOf(Array);

    expect(response.body.find((e) => e._id === category._id)).toBeTruthy();
  });

  it('should update the category', async () => {
    category.code = 'New code';
    category.name = 'New name';

    const responseUpdate = await request(app)
      .put(`/categories/${category._id}`)
      .send(category);
    expect(responseUpdate.status).toBe(200);

    expect(responseUpdate.body.code).toBe(category.code);
    expect(responseUpdate.body.name).toBe(category.name);

    expect(responseUpdate.body).toHaveProperty('_id');
    expect(responseUpdate.body).toHaveProperty('createdAt');
    expect(responseUpdate.body).toHaveProperty('updatedAt');

    expect(responseUpdate.body._id).toBe(category._id);

    const responseGet = await request(app).get(`/categories/${category._id}`);
    expect(responseGet.status).toBe(200);

    expect(responseUpdate.body.code).toBe(category.code);
    expect(responseUpdate.body.name).toBe(category.name);
  });

  it('should delete the category', async () => {
    const responseUpdate = await request(app).delete(
      `/categories/${category._id}`
    );
    expect(responseUpdate.status).toBe(200);

    const responseGet = await request(app).get(`/categories/${category._id}`);
    expect(responseGet.status).toBe(404);
  });
});

describe('CRUD Category - Exceptions expected', () => {
  const category = {
    code: 'A12',
    name: 'Esportes',
  };

  it('should send a error when trying to create a category without a mandatory field', async () => {
    const temporaryCategory = Object.assign(category);
    temporaryCategory.code = undefined;
    let response = await request(app)
      .post('/categories')
      .send(temporaryCategory);
    expect(response.status).toBe(400);

    temporaryCategory.code = category.code;
    temporaryCategory.name = undefined;
    response = await request(app).post('/categories').send(temporaryCategory);
    expect(response.status).toBe(400);
  });

  it('should send a error when trying to update a category that does not exist', async () => {
    let response = await request(app).put(`/categories/invalidId`);
    expect(response.status).toBe(400);

    response = await request(app).put(`/categories/000000000000000000000000`);
    expect(response.status).toBe(404);
  });

  it('should send a error when trying to delete a category that does not exist', async () => {
    let response = await request(app).delete(`/categories/invalidId`);
    expect(response.status).toBe(400);

    response = await request(app).delete(
      `/categories/000000000000000000000000`
    );
    expect(response.status).toBe(404);
  });

  it('should send a error when trying to find a category that does not exist', async () => {
    let response = await request(app).get(`/categories/invalidId`);
    expect(response.status).toBe(400);

    response = await request(app).get(`/categories/000000000000000000000000`);
    expect(response.status).toBe(404);
  });
});
