# Space-news-codesh

A rest API application developed in Node Js. Used to controll products and their
categories.

/doc to see the docs of the application

# Technologies

- Node
- MongoDb
- Swagger
- Jest

# Installation

You need a Atlas account for the mongoDb (https://docs.atlas.mongodb.com/getting-started/)
and configure the MONGO_URL variable in the .env file.

You can also change the
application port, the default is 3000.

## - yarn install

Download and install all the dependencies of the project.

## - yarn start

Will build and start the application.

## - yarn test

Will run the tests.
