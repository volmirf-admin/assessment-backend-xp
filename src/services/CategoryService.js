import Category from '../models/Category';

export default {
  getCategories: async (pageSize, pageNumber) => {
    const categories = await Category.find()
      .skip(pageNumber > 0 ? (pageNumber - 1) * pageSize : 0)
      .limit(pageSize);
    return categories;
  },
  getCategory: async (categoryId) => {
    const category = await Category.findById(categoryId);
    return category;
  },
  createCategory: async (category) => {
    const newCategory = await Category.create(category);
    return newCategory;
  },
  updateCategory: async (categoryId, category) => {
    const updatedCategory = await Category.findByIdAndUpdate(
      categoryId,
      category,
      {
        new: true,
      }
    );
    return updatedCategory;
  },
  deleteCategory: async (categoryId) => {
    return await Category.findByIdAndRemove(categoryId);
  },
};
