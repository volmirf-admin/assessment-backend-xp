import Product from '../models/Product';

export default {
  getProducts: async (pageSize, pageNumber) => {
    const products = await Product.find()
      .skip(pageNumber > 0 ? (pageNumber - 1) * pageSize : 0)
      .limit(pageSize)
      .populate(['categories']);
    return products;
  },
  getProduct: async (productId) => {
    const product = await Product.findById(productId).populate(['categories']);
    return product;
  },
  createProduct: async (product) => {
    let newProduct = await Product.create(product);
    newProduct = await newProduct.populate(['categories']);
    return newProduct;
  },
  updateProduct: async (productId, product) => {
    const updatedProduct = await Product.findByIdAndUpdate(productId, product, {
      new: true,
    }).populate(['categories']);
    return updatedProduct;
  },
  deleteProduct: async (productId) => {
    return await Product.findByIdAndRemove(productId);
  },
};
