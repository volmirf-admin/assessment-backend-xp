import { ProductService } from '../services';

export default {
  getProducts: async (req, res) => {
    try {
      const products = await ProductService.getProducts(
        req.query.page_size,
        req.query.page_num
      );

      res.send(products);
    } catch (error) {
      res.status(400).send('Error loading products');
    }
  },
  getProduct: async (req, res) => {
    try {
      const product = await ProductService.getProduct(req.params.productId);
      if (product === null) {
        return res.status(404).send('Product not found');
      }
      res.send(product);
    } catch (error) {
      res.status(400).send('Error loading product');
    }
  },
  createProduct: async (req, res) => {
    try {
      const product = await ProductService.createProduct(req.body);
      res.status(201).send(product);
    } catch (error) {
      res.status(400).send('Error creating new product');
    }
  },
  updateProduct: async (req, res) => {
    try {
      const product = await ProductService.updateProduct(
        req.params.productId,
        req.body
      );
      if (product === null) {
        return res.status(404).send('Product not found');
      }
      res.send(product);
    } catch (error) {
      res.status(400).send('Error creating new product');
    }
  },
  deleteProduct: async (req, res) => {
    try {
      const product = await ProductService.deleteProduct(req.params.productId);
      if (product === null) {
        return res.status(404).send('Product not found');
      }
      res.send();
    } catch (error) {
      res.status(400).send('Error deleting product');
    }
  },
};
