import { CategoryService } from '../services';

export default {
  getCategories: async (req, res) => {
    try {
      const categories = await CategoryService.getCategories(
        req.query.page_size,
        req.query.page_num
      );

      res.send(categories);
    } catch (error) {
      res.status(400).send('Error loading categories');
    }
  },
  getCategory: async (req, res) => {
    try {
      const category = await CategoryService.getCategory(req.params.categoryId);
      if (category === null) {
        return res.status(404).send('Category not found');
      }
      res.send(category);
    } catch (error) {
      res.status(400).send('Error loading category');
    }
  },
  createCategory: async (req, res) => {
    try {
      const category = await CategoryService.createCategory(req.body);
      res.status(201).send(category);
    } catch (error) {
      res.status(400).send('Error creating new category');
    }
  },
  updateCategory: async (req, res) => {
    try {
      const category = await CategoryService.updateCategory(
        req.params.categoryId,
        req.body
      );
      if (category === null) {
        return res.status(404).send('Category not found');
      }
      res.send(category);
    } catch (error) {
      res.status(400).send('Error updating category');
    }
  },
  deleteCategory: async (req, res) => {
    try {
      let categoryDeleted = await CategoryService.deleteCategory(
        req.params.categoryId
      );
      if (categoryDeleted === null) {
        return res.status(404).send('Category not found');
      }
      res.send();
    } catch (error) {
      res.status(400).send('Error deleting category');
    }
  },
};
