const fs = require('fs');
const swaggerAutogen = require('swagger-autogen')();

const outputFile = './docs/swagger_output.json';
const endpointsFiles = fs.readdirSync('./src/routes');

const doc = {
  info: {
    version: '1.0.0',
    title: 'Space News Codesh API',
    description: 'Documentation of Space News Codesh API.',
  },
  host: 'localhost:3000',
  basePath: '/',
  schemes: ['http'],
  consumes: ['application/json'],
  produces: ['application/json'],
  tags: [
    {
      name: 'Category',
      // description: 'Endpoints',
    },
    {
      name: 'Product',
    },
  ],
  // securityDefinitions: {
  //   api_key: {
  //     type: 'apiKey',
  //     name: 'api_key',
  //     in: 'header',
  //   },
  //   petstore_auth: {
  //     type: 'oauth2',
  //     authorizationUrl: 'https://petstore.swagger.io/oauth/authorize',
  //     flow: 'implicit',
  //     scopes: {
  //       read_pets: 'read your pets',
  //       write_pets: 'modify pets in your account',
  //     },
  //   },
  // },
  definitions: {
    Category: {
      _id: '6217951e81f99f25077865fe',
      code: 'A12',
      name: 'Esportes',
    },
    NewCategory: {
      $code: 'A12',
      $name: 'Esportes',
    },
    Product: {
      _id: '6217951e81f99f25077865fe',
      name: 'A12',
      sku: '42254-011',
      price: 89.9,
      description: 'Bola de Basquete',
      quantity: 22,
      categories: [{ $ref: '#/definitions/Category' }],
    },
    NewProduct: {
      name: 'A12',
      sku: '42254-011',
      price: 89.9,
      description: 'Bola de Basquete',
      quantity: 22,
      categories: [{ $ref: '#/definitions/Category' }],
    },
  },
};

swaggerAutogen(
  outputFile,
  endpointsFiles.map((e) => './src/routes/' + e),
  doc
);
