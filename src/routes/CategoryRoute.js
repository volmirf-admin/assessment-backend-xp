import { Router } from 'express';
import { CategoryController } from '../controller';

const routes = Router();

routes.get(
  '/categories',
  CategoryController.getCategories
  /*
    #swagger.tags = ['Category']
    #swagger.parameters['page_size'] = {
      in: 'query',
      description: 'Number of documents per page.',
      type: 'number'
    }
    #swagger.parameters['page_num'] = {
      in: 'query',
      description: 'Number of current page.',
      type: 'number'
    }
    #swagger.responses[200] = { 
      schema: [{ $ref: "#/definitions/Category" }],
      description: 'Category list.'
    }
  */
);

routes.get(
  '/categories/:categoryId',
  CategoryController.getCategory
  /*
    #swagger.tags = ['Category']
    #swagger.parameters['categoryId'] = { description: 'Category id.'}
    #swagger.responses[200] = { 
      schema: { $ref: "#/definitions/Category" },
      description: 'Category found.'
    }
  */
);

routes.post(
  '/categories',
  CategoryController.createCategory
  /*
    #swagger.tags = ['Category']
    #swagger.parameters['newCategory'] = {
      in: 'body',
      description: 'Category that will be created.',
      required: true,
      schema: { $ref: "#/definitions/NewCategory" }
    }
    #swagger.responses[201] = { 
      schema: { $ref: "#/definitions/Category" },
      description: 'Category created.'
    }
  */
);

routes.put(
  '/categories/:categoryId',
  CategoryController.updateCategory
  /*
    #swagger.tags = ['Category']
    #swagger.parameters['categoryId'] = { description: 'Category id.'}
    #swagger.parameters['updatedCategory'] = {
      in: 'body',
      description: 'Category that will be updated.',
      required: true,
      schema: { $ref: "#/definitions/NewCategory" }
    }
    #swagger.responses[200] = { 
      schema: { $ref: "#/definitions/NewCategory" },
      description: 'Category updated.'
    }
  */
);
routes.delete(
  '/categories/:categoryId',
  CategoryController.deleteCategory
  /*
    #swagger.tags = ['Category']
    #swagger.parameters['categoryId'] = { description: 'Category id.'}
  */
);

export default routes;
