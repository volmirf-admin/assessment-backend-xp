import { Router } from 'express';
import CategoryRoute from './CategoryRoute';
import ProductRoute from './ProductRoute';

const routes = Router();

export default [routes, CategoryRoute, ProductRoute];
