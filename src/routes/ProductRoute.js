import { Router } from 'express';
import { ProductController } from '../controller';

const routes = Router();

routes.get(
  '/products',
  ProductController.getProducts
  /*
    #swagger.tags = ['Product']
    #swagger.parameters['page_size'] = {
      in: 'query',
      description: 'Number of documents per page.',
      type: 'number'
    }
    #swagger.parameters['page_num'] = {
      in: 'query',
      description: 'Number of current page.',
      type: 'number'
    }
    #swagger.responses[200] = { 
      schema: [{ $ref: "#/definitions/Product" }],
      description: 'Product list.'
    }
  */
);

routes.get(
  '/products/:productId',
  ProductController.getProduct
  /*
    #swagger.tags = ['Product']
    #swagger.parameters['productId'] = { description: 'Product id.'}
    #swagger.responses[200] = { 
      schema: { $ref: "#/definitions/Product" },
      description: 'Product found.'
    }
  */
);

routes.post(
  '/products',
  ProductController.createProduct
  /*
    #swagger.tags = ['Product']
    #swagger.parameters['newProduct'] = {
      in: 'body',
      description: 'Product that will be created.',
      required: true,
      schema: { $ref: "#/definitions/NewProduct" }
    }
    #swagger.responses[201] = { 
      schema: { $ref: "#/definitions/Product" },
      description: 'Product created.'
    }
  */
);

routes.put(
  '/products/:productId',
  ProductController.updateProduct
  /*
    #swagger.tags = ['Product']
    #swagger.parameters['productId'] = { description: 'Product id.'}
    #swagger.parameters['updatedProduct'] = {
      in: 'body',
      description: 'Product that will be updated.',
      required: true,
      schema: { $ref: "#/definitions/NewProduct" }
    }
    #swagger.responses[200] = { 
      schema: { $ref: "#/definitions/NewProduct" },
      description: 'Product updated.'
    }
  */
);
routes.delete(
  '/products/:productId',
  ProductController.deleteProduct
  /*
    #swagger.tags = ['Product']
    #swagger.parameters['productId'] = { description: 'Product id.'}
  */
);

export default routes;
