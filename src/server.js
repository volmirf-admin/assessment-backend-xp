import 'core-js/stable';
import 'regenerator-runtime/runtime';

import app from './app.js';

app.listen(process.env.PORT || 3000);
